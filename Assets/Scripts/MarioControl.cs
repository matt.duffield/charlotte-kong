﻿using UnityEngine;
using System.Collections;

public class MarioControl : MonoBehaviour {
	
	[HideInInspector]
	public bool facingRight = false;			// For determining which way the player is currently facing.
	[HideInInspector]
	public string axisH = "Horizontal";
	[HideInInspector]
	public string axisV = "Vertical";
	
	public float speed = 8;
	private Animator anim;					// Reference to the player's animator component.
	
	
	void Awake()
	{
		// Setting up references.
        anim = GetComponent<Animator>();
	}
	
	// used for input
	void Update()
	{
	}
	
	// used for add forces
	// Update is called once per frame
	void FixedUpdate () 
	{
		// Cache the horizontal input.
		float h = Input.GetAxis(axisH);
		// Cache the horizontal input.
		float v = Input.GetAxis(axisV);
		
		// The Speed animator parameter is set to the absolute value of the horizontal input.
        anim.SetFloat("Speed", Mathf.Abs(h));
				
		transform.position += transform.right * h * speed * Time.deltaTime;
		transform.position += transform.up * v * speed * Time.deltaTime;
		
		// If the input is moving the player right and the player is facing left...
		if(h < 0 && facingRight) {
			// ... flip the player.
			Flip();
		}
		// Otherwise if the input is moving the player left and the player is facing right...
		else if(h > 0 && !facingRight) {
			// ... flip the player.
			Flip();
		}
	}
	
	void Flip ()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
	//	void OnTriggerEnter2D(Collider2D coll) {
	//		Debug.Log(coll.tag);
	//		if (coll.tag == "ground") {
	//			Destroy(coll.gameObject);
	//			Debug.Log("Destroyed object.");
	//		}
	//	}
	
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "barrel") {
			Destroy(coll.gameObject);
            Destroy(gameObject);
		}
	}
};
